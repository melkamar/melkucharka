import logging
from typing import *

from pelican import signals
from pelican.readers import BaseReader

import yaml

log = logging.getLogger(__name__)


class YamlRecipeParser:
    def __init__(self, filename: str):
        self._filename = filename
        with open(filename) as f:
            self._content = yaml.safe_load(f)

    def read_required(self, option: Union[str, List[str]]):
        """
        Read a value from the yaml file. If it does not exist, raise KeyError.

        :param option: if string, read a top-level value. If a list, read the value from a nested struct.
        :return:
        """
        try:
            if type(option) is str:
                return self._content[option]

            if type(option) is list:
                cur = self._content
                for k in option:
                    cur = cur[k]
                return cur
            return ValueError(f'Expected a str or list, got {type(option)}: {option}')
        except KeyError:
            raise KeyError(
                f'Required option "{option}" is missing from {self._filename}')

    def read_optional(self, option: Union[str, List[str]], default=None, none_as_default=False):
        """
        Read a value from the yaml file. If it does not exist, return a default val.

        :param option: if string, read a top-level value. If a list, read the value from a nested struct.
        :param default:
        :param none_as_default:
        :return:
        """
        try:
            result = self._content[option]
            if result is None and none_as_default:
                return default

            return result
        except KeyError:
            return default


# Create a new reader class, inheriting from the pelican.reader.BaseReader
class NewReader(BaseReader):
    enabled = True  # Yeah, you probably want that :-)

    # The list of file extensions you want this reader to match with.
    # If multiple readers were to use the same extension, the latest will
    # win (so the one you're defining here, most probably).
    file_extensions = ['yml', 'yaml']

    # You need to have a read method, which takes a filename and returns
    # some content and the associated metadata.
    def read(self, filename):
        parser = YamlRecipeParser(filename)

        tags = parser.read_optional('tags', (), none_as_default=True)

        metadata = {
            'title': parser.read_required('title'),
            'date': parser.read_optional('date', '2021-01-01 08:00'),
            'modified': parser.read_optional('modified', '2021-01-01 08:00'),
            'tags': "; ".join(tags),
            'title_image': parser.read_required('image'),
            'ingredients': parser.read_required('ingredients'),
            'directions': parser.read_required('directions'),
            'notes': parser.read_optional('notes', (), none_as_default=True),
            'references': parser.read_optional('references', ()),
            'time': {
                'active': parser.read_required(['time', 'active']),
                'total': parser.read_required(['time', 'total']),
            },
            'portions': parser.read_required('portions'),
            'text': parser.read_optional('text')
        }

        parsed = {}
        for key, value in metadata.items():
            parsed[key] = self.process_metadata(key, value)

        return "[ERROR] No unstructured content should be used in this theme", parsed


def add_reader(readers):
    readers.reader_classes['yeah'] = NewReader


# This is how pelican works.
def register():
    signals.readers_init.connect(add_reader)
