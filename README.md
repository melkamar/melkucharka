```bash
# Pelican core
python -m pip install 'pelican[markdown]'

# CSS asset pipeline
python -m pip install pelican-webassets libsass cssmin

# Image processing
python -m pip install pelican-image-process

# Additional deps
python -m pip install pyyaml
```

## TODOs
- auto-read date created and modified from file metadata in reader
- port fonts
- gitlab ci pipeline
- replace the original jekyll with this
- add metadata about time required (prep, cook)
- add metadata about portions

## Recipe data model

```yaml
# The title of the recipe
title:  "Tadka dal (indické curry z červené čočky)"

# The main image of the recipe. Null for no image (placeholder art will be shown).
image: tadka-dal.jpeg

# The time it takes to make the recipe
time:
  active: 30 min  # Time actively spent on the recipe - prepping, cooking
  total: 1 hour  # Time it takes to make the recipe, start to end, incl. waiting time

# Number of portions provided by the recipe
portions: 6

# Markdown string. Shown in the heading card.
text: >
  Some introduction text. Soppy life story, that kind of stuff.

# List of markdown strings. Shows as a list of tags
tags: 
- Foo 

# List of markdown strings. Shown as a list of ingredients.
ingredients:
- name: Červená čočka
  amount: 2 hrnky # The amount of the ingredient, incl. units
  notes: # Additional info, shown as an on-hover popup
    - Some clever note
    - Lifehack

# List of markdown strings. Shown as a list of steps.
directions: []

# List of markdown strings. 
notes: []


```
