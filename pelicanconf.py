#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Martin Melka'
SITENAME = 'Melkuchařka'
SITEURL = 'http://localhost:8000'

PATH = 'content'

TIMEZONE = 'Europe/Prague'

DEFAULT_LANG = 'cs'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'https://getpelican.com/'),
         ('Python.org', 'https://www.python.org/'),
         ('Jinja2', 'https://palletsprojects.com/p/jinja/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = 'melkamartheme'

# pelican-image-process
IMAGE_PROCESS = {
    "thumb": {
        "type": "image",
        "ops": ["scale_out 150 150 True", "crop 0 0 150 150"],
        # TODO how to crop to center?
    },
    "article-heading-image": {
        "type": "image",
        "ops": ["scale_in 960 960 True"],
    },
    "site-logo": {
        "type": "image",
        "ops": ["scale_in 560 99999 True"],
    },
    "recipe-thumbnail": {
        "type": "image",
        "ops": ["scale_in 400 400 True"],
    }
}

PLUGIN_PATHS = ['plugins/']
PLUGINS = ['webassets', 'image_process', 'melkamarplugin']

LOAD_CONTENT_CACHE = False
